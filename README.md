---
layout: markdown_page
title: "Brie Carranza's README"
---

## 🦄 Brie Carranza's README

**Brie Carranza, Staff Support Engineer (AMER)** This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team. Please feel free to contribute to this page by opening a merge request. :rainbow:

## About me

👋 Hello, world! My name is **Brie** Carranza. I am a **Staff Support Engineer** at GitLab. I joined GitLab in May 2020, just after [GitLab 13.0](https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/) was released. 🦊 💜 I've been a member of the GitLab Support team during all my years at GitLab. Every year, I reflect a bit:

- [A look back at one year at GitLab](https://brie.dev/year-at-gitlab/)
- [A second year at GitLab](https://brie.dev/second-year-at-gitlab/)
- [Three Years at GitLab 🦊💜](https://brie.dev/three-years-at-gitlab/)
- [4️⃣ Four Things about my Fourth Year at GitLab](https://brie.dev/four-years-gitlab/)

I spend my time working to understand the cause(s) of unexpected behavior and helping folks make the best use of GitLab. I fell in love with GitLab as a product before joining the team so it's super fun and exciting to get to help others experience how cool and fun it can be! I enjoy learning, thinking, writing and discussing [general purpose troubleshooting principles](https://brie.dev/troubleshooting/).

Prior to joining GitLab, I worked in the School of Computer Science at Carnegie Mellon University for many years.

### 🎥 Videos

- [Bruno Freitas Interviews Brie Carranza | GitLab Support](https://www.youtube.com/watch?v=7hUIyTX_0Ow)
- [✨ Brie Carranza Interviews Manuel Grabowski | GitLab Support](https://www.youtube.com/watch?v=OxR_GTTawLE)
- [Interview with GitLab Support Engineer Brie Carranza](https://www.youtube.com/watch?v=d2vo_--lX-M) | I was interviewed by my colleague Sharif Bennet in January 2021 interview [on YouTube](https://youtu.be/d2vo_--lX-M). Skip straight to my [advice to my 20 year-old self](https://youtu.be/d2vo_--lX-M?t=835) and others who are new to tech. In short:

  - Have a plan but understand opportunities come from where you may not expect them. Be OK with changes to your plan. 
  - Cultivate a sense of curiosity. 

## 🔗 Related pages

  * :notebook: My [notes](https://notes.brie.dev) site has _years_ worth of loose notes I have taken while working on tickets during my years at GitLab.
  * [bcarranza.gitlab.io/readme](https://bcarranza.gitlab.io/readme/) - This README served via GitLab Pages
  * [gitlab.com/brie](https://gitlab.com/brie) - my personal GitLab profile
  * [my cat Plop](https://about.gitlab.com/company/team-pets/#305-plop-beauregard) - on the GitLab Team Pets page

## 🛟 How you can help me (help you)

  * 🔮 Be clear about how I can help you. If you want to brainstorm and form a plan: I'm in! If you want someone to just _listen_: I'm in!
  * Let me know what you'd like me to **START**, **STOP** or **CONTINUE** doing. 
  * 🙏 I greatly appreciate constructive feedback. I do take it to heart. Please deliver it with kindness.
  * ⏯️ Don't wait. If I can help or if you need something from me: don't wait, let me know!
  * _Make suggestions for what should go here._

## 💼 My working style

  * While I greatly enjoy 🍐 pairing, I appreciate having a few minutes to review the ticket async before we get started.
  * This [ticket-talk](https://gitlab.com/bcarranza/ticket-talk) project contains the template I use for some tickets and some notes on how I incorporate that template and [Zettlr](https://zettlr.com) into my workflow. 
    * This workflow was replaced with [Fieldnotes](https://about.gitlab.com/handbook/support/workflows/fieldnote_issues.html). :notebook_with_decorative_cover: 
  * When confronted with a new challenge, I aim to sort things roughly into "what I know about" and "what I don't know about". From there, I aim to find the things in the "what I don't know about" category that interact with the things that I do know, break them into atomic units, learn about them and build them into the "what I know (enough) about" category. 
  * I sometimes forget about things that I know about. 
  * I am more of a generalist than a specialist. I am always interested in doing a deep dive and learning a reasonable amount of specifics on worthwhile topics. 
  * Personality: [Mediator (INFP-T)](https://www.16personalities.com/infp-personality) - This does not dictate everything about my personality but it might prove a useful starting point.   
  * When taking a ticket, my first step is always making sure that there is a clear problem statement. All parties must be working toward solving the same problem. In my experience, working to define the problem clearly increases efficiency (by reducing unnecessary back and forths later in the ticket's lifecycle). 
  * I learn by doing. Test environments where I can break and test things are critical to helping me diagnose and understand a problem. 
  * While I greatly enjoy synchronous brainstorming, I think I do my best troubleshooting and writing solo and asynchronously. I genuinely enjoy watching other people troubleshoot and troubleshooting with them as I am always interested in picking up tips and tricks from my peers. (If you are reading this, please consider scheduling a coffee chat or pairing session. I'd love to get to know you.) If you have learned something the hard way, I would be very happy to benefit from your experience and learn it the easy way from you.  

## ✨ How I work and think

I am a 'perpetual learner'. I tend to value "academic" tools and find ways to incorporate these ways of thinking into daily work.

### Eisenhower Matrix

I often find the [Eisenhower Matrix](https://academicsuccess.ucf.edu/sarc/wp-content/uploads/sites/31/2020/12/Eisenhower-Matrix-Fillable.pdf) useful.

## What I assume about others

  * I assume positive intent. I aim to act with positive intent. 
  * I assume patience. I aim to be patient. 
  * I assume you will ask me to clarify if I say something that is not clear. I aim to check for understanding and make sure the audience (regardless of size) is onboard. 


## What I want to earn

[**Consider 3-5 bullets on your goals for earning things like trust and respect, or a broader understanding of new topics. This enables others to understand what motivates you.**]

## Communicating with me

[**Consider 5-10 bullets on your communication preferences. This includes traditional styles such as verbal, textual, and visual, but you are encouraged to be precise. You can mention things like routine, availability, your travel habits, etc. This helps others understand why you communicate in the manner than you do, and it enables them to tailor their communication in a way that resonates most with you.**]

My preference for communication is via GitLab TODOs, Slack or email, in that order. I am very easy-going so feel free to disturb that order at your leisure. 

🎗️ I am to be responsive. Don't hesitate to ping me or DM me if you need something more quickly. Feel free to follow up.

When discussing technical components, I will almost always say yes to hearing more detail about what you are explaining to me. I always have more curiosity than I have time. I'll let you know if a conversation is too far in the weeds to be an effective use of time.

## Strengths/Weaknesses

Words that are frequently used to describe me/my work:

  - enthusiastic: I think that a great many things are exciting! 
  - detailed

### Weaknesses

#### Iteration

Iteration [is hard](https://about.gitlab.com/blog/2020/02/04/power-of-iteration/). I am newish to GitLab and am learning to iterate and assemble smaller scopes for work.

Eight months in: iteration ~~is hard~~ requires intention. 

Two years in: iteration can be done. It requires intention. It's not easy. It sometimes feels like backward progress. It is worthwhile.

_Almost_ four years in: iteration is really powerful. I'm getting better at it but I'll always back working on it.

## Tech
  * My first programming language is Python. If I need to do something quickly, I will do it in Python or Go. 
  * I tend towards CLI-based tools and solutions.
  * CLI Text Editors: I default to `vim` unless I am making use of the [excellent org-mode](https://orgmode.org/) in `emacs`.
  * Markdown Editors: [Codium](https://dev.to/0xdonut/why-and-how-you-should-to-migrate-from-visual-studio-code-to-vscodium-j7d) and [Atom](https://atom.io)
  * IDEs: PyCharm, RubyMine, GoLand, Codium
  * Bruno - For testing out API calls and then automagically converting a refined `cURL` command to a block of Python/Go/Ruby/whatever. 
  * SSH magic: SSH has so many seemingly simple components that one can build on. Tell me the latest fun thing you did with SSH! (Boring SSH solutions are quite fun!)
  * I have been experimenting with different static site generators. I'm rather fond of Gridsome but I love most things [JAM (Javascript + APIs + Markup) stack](https://jamstack.org/). 🚀 I am using [Astro](https://astro.build) by default as of 2024.

## Characteristics
  * ambitious - I always want to learn and know more. The hard part is deciding which things to focus on and which to defer or decline.
  * curious - I want to know the answer. 
  * enthusiastic - I am a naturally enthusiastic and energetic person. [Burn out](https://www.helpguide.org/articles/stress/burnout-prevention-and-recovery.htm) is real so I manage my enthusiasm to prevent this. 
  * perpetual learner - I enjoy learning new things, whether related to technology or not.
  * planner - I am a planner. I have been described by others as "very organized". My personal organizational approach is the result of developing my organizaitonal skills over time with deliberation and intent. (I am constantly developing my personal workflow and evaluating new tools to replace existing ones. I would love to have a coffee chat to talk about your favorite productivity tools (and/or mine)!)

### Clifton StrengthsFinder

I completed the CliftonStrengths assessment in late 2023. I lead with **Strategic Thinking**. My top 5️⃣  five strengths are **Restorative**, **Learner**, **Intellection**, **Individualization** and **Harmony**.

## 🧭 Principles

  * The solution should be as simple as possible but **no** simpler. 
  * Back up the data. Perform meaningful tests of the data restoration process.
  * It is 🆗 OK to not know what to do. Plan to be in situations where you don't know what to do. Start with what you know, form a plan, ask for help.
  * In difficult situations, sometimes the best thing to do is to set your perspective aside, assume the other party is a rational actor and consider what they might want out of the situation, what their limitations might be, et cetera. Be kind. Remember that there is another human person on the other side of the keyboard (unless it's a dog but you should still be kind to dogs). 
